// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

  import {
    getDatabase,
    onValue,
    ref,
    set,
    child,
    get,
    update,
    remove
  }
  from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";


  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyAHK3txDeCMJ2UhsUaoQAE32kTaPD3A6d4",
    authDomain: "webcamisaslmx.firebaseapp.com",
    projectId: "webcamisaslmx",
    storageBucket: "webcamisaslmx.appspot.com",
    messagingSenderId: "538259902329",
    appId: "1:538259902329:web:9eb4eb12c41e51dccda9a8"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase();

  var btnAgregar = document.getElementById('btnAgregar');
  var btnBuscar = document.getElementById('btnBuscar');
  var btnActualizar = document.getElementById('btnActualizar');
  var btnBorrar = document.getElementById('btnBorrar');
  var btnTodos = document.getElementById('btnTodos');

//Insertar variables inputs
var articulos = "";
var nombre = "";
var descripcion = "";
var precio = "";
var existencia = "";


function leerInputs() {
  articulos = document.getElementById('articulos').value;
  nombre = document.getElementById('nombre').value;
  descripcion = document.getElementById('descripcion').value;
  precio = document.getElementById('precio').value;
  existencia = document.getElementById('existencia').value;

  //alert("  Matricula  " + matricula + "  Nombre  " + nombre + "  Carrera  " + carrera + "  Genero  " + genero);
}

function insertarDatos() {
    leerInputs();
    set(ref(db, 'Productos/' + articulos), {
      Nombre: nombre,
      Descripcion: descripcion,
      Precio: precio,
      Existencia: existencia
    }).then((res) => {
        alert("Se Inserto con exito")
    }).catch((error) => {
        alert("Surgio un error " + error)
    });

}

// mostrar datos
function mostrarDatos() {
  leerInputs();
  const dbref = ref(db);
  get(child(dbref, 'Productos/' + articulos)).then((snapshot) => {
      if (snapshot.exists()) {
          nombre = snapshot.val().Nombre;
          descripcion = snapshot.val().Descripcion;
          precio = snapshot.val().Precio;
          existencia = snapshot.val().Existencia;
          escribirInputs();
      } else {
          alert("No se encontro el registro ");
      }
  }).catch((error) => {
      alert("Surgio un error " + error);
  });
}


function actualizar() {
  leerInputs();
  update(ref(db, 'Productos/' + articulos), {
      Nombre: nombre,
      Descripcion: descripcion,
      Precio: precio,
      Existencia: existencia
  }).then(() => {
      alert("Se realizo actualizacion");
      mostrarAlumnos();
  }).catch(() => {
      alert("Causo Error " + error);
  });
}

function borrar() {
  leerInputs();
  remove(ref(db, 'Productos/' + articulos)).then(() => {
      alert("Se borro con exito");
      mostrarProductos();
  }).catch(() => {
      alert("Causo Error " + error);
  });
}

function mostrarProductos() {
  const db = getDatabase();
  const dbRef = ref(db, 'Productos');

  onValue(dbRef, (snapshot) => {
      lista.innerHTML = ""
      snapshot.forEach((childSnapshot) => {

          const childKey = childSnapshot.key;
          const childData = childSnapshot.val();

          lista.innerHTML = "<div class='campo'>" + lista.innerHTML + childKey + " | Nombre: " + childData.Nombre + " | Descripcion: " + childData.Descripcion + " | $" + childData.Precio + " | En existencia: " + childData.Existencia + "<br></div>";
          console.log(childKey + ":");
          console.log("$" + childData.precio)
      });
  }, {
      onlyOnce: true
  });
}


function escribirInputs() {
  document.getElementById('articulos').value = articulos;
  document.getElementById('precio').value = precio;
  document.getElementById('existencia').value = existencia;
}

//btnAgregar.addEventListener('click', leerInputs);
btnAgregar.addEventListener('click', insertarDatos);
btnConsultar.addEventListener('click', mostrarDatos);
btnActualizar.addEventListener('click', actualizar);
btnBorrar.addEventListener('click', borrar);
btnTodos.addEventListener('click', mostrarProductos);
