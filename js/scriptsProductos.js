// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

  import {
    getDatabase,
    onValue,
    ref,
    set,
    child,
    get,
    update,
    remove
  }
  from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";


  // Your web app's Firebase configuration
  const firebaseConfig = {
    apiKey: "AIzaSyAHK3txDeCMJ2UhsUaoQAE32kTaPD3A6d4",
    authDomain: "webcamisaslmx.firebaseapp.com",
    projectId: "webcamisaslmx",
    storageBucket: "webcamisaslmx.appspot.com",
    messagingSenderId: "538259902329",
    appId: "1:538259902329:web:9eb4eb12c41e51dccda9a8"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase();

  let nombre1 = document.getElementById("nombre1");
  let descripcion1 = document.getElementById("descripcion1");
  let precio1 = document.getElementById("precio1");
  let existencia1 = document.getElementById("existencia1");
  let actualizarExistencia1 = 0;

  let nombre2 = document.getElementById("nombre2");
  let descripcion2 = document.getElementById("descripcion2");
  let precio2 = document.getElementById("precio2");
  let existencia2 = document.getElementById("existencia2");
  let actualizarExistencia2 = 0;

  let nombre3 = document.getElementById("nombre3");
  let descripcion3 = document.getElementById("descripcion3");
  let precio3 = document.getElementById("precio3");
  let existencia3 = document.getElementById("existencia3");
  let actualizarExistencia3 = 0;

  window.onload = function cargarDatos() {
    const dbref = ref(db);
    get(child(dbref, 'Productos/' + "1")).then((snapshot) => {  //Producto 1
        if (snapshot.exists()) {
            nombre1.innerHTML = snapshot.val().Nombre;
            descripcion1.innerHTML = snapshot.val().Descripcion;
            precio1.innerHTML = "Precio: $" + snapshot.val().Precio;
            existencia1.innerHTML = "Unidades Disponibles: " + snapshot.val().Existencia;
        
              actualizarExistencia1 = snapshot.val().existencia
          }
    })

    get(child(dbref, 'Productos/' + "2")).then((snapshot) => { //Producto 2
        if (snapshot.exists()) {
            nombre2.innerHTML = snapshot.val().Nombre;
            descripcion2.innerHTML = snapshot.val().Descripcion;
            precio2.innerHTML = "Precio: $" + snapshot.val().Precio;
            existencia2.innerHTML = "Unidades Disponibles: " + snapshot.val().Existencia;

            actualizarExistencia2 = snapshot.val().existencia
        }
    })

    get(child(dbref, 'Productos/' + "3")).then((snapshot) => { //Producto 3
        if (snapshot.exists()) {
            nombre3.innerHTML = snapshot.val().Nombre;
            descripcion3.innerHTML = snapshot.val().Descripcion;
            precio3.innerHTML = "Precio: $" + snapshot.val().Precio;
            existencia3.innerHTML = "Unidades Disponibles: " + snapshot.val().Existencia;

            actualizarExistencia3 = snapshot.val().existencia

        }
    })
  }


  function compra1() {

    if(actualizarExistencia1>0){
    actualizarExistencia1 = actualizarExistencia1 -1;
    
    update(ref(db, 'Productos/' + "Jersey Local: America"), {
      existencia: actualizarExistencia1
    })
    alert("Compra Exitosa")
    location.reload();
    }else{
      alert("No hay camisas disponibles")
    }
  }

  function compra2() {

    if(actualizarExistencia2>0){
    actualizarExistencia2 = actualizarExistencia2 -1;
    
    update(ref(db, 'Productos/' + "Jersey Local: MazatlanFC"), {
      existencia: actualizarExistencia2
    })
    alert("Compra Exitosa")
    location.reload();
    }else{
      alert("No hay camisas disponibles")
    }
  }

  function compra3() {

    if(actualizarExistencia3>0){
    actualizarExistencia3 = actualizarExistencia3 -1;
    
    update(ref(db, 'Productos/' + "Jersey Local: Puebla"), {
      existencia: actualizarExistencia3
    })
    alert("Compra Exitosa")
    location.reload();
    }else{
      alert("No hay camisas disponibles")
    }
  }

  bntCompra1.addEventListener('click', compra1);
  bntCompra2.addEventListener('click', compra2);
  bntCompra3.addEventListener('click', compra3);
